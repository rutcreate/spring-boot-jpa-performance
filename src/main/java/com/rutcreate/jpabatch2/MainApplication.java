package com.rutcreate.jpabatch2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.EntityManager;
import javax.sql.DataSource;

@SpringBootApplication
public class MainApplication
{
    public static void main(String args[])
    {
        SpringApplication.run(MainApplication.class, args);
    }
}
