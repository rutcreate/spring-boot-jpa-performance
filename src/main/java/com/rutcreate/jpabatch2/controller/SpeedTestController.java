package com.rutcreate.jpabatch2.controller;

import com.rutcreate.jpabatch2.service.TestDeleteService;
import com.rutcreate.jpabatch2.service.TestInsertService;
import com.rutcreate.jpabatch2.service.TestSelectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

@RestController
@RequestMapping("test")
public class SpeedTestController
{
    @Autowired
    private TestInsertService testInsertService;

    @Autowired
    private TestDeleteService testDeleteService;

    @Autowired
    private TestSelectService testSelectService;

    @RequestMapping("")
    @Transactional
    public String home() throws IOException
    {
        Resource resource = new ClassPathResource("templates/index.html");
        InputStream resourceInputStream = resource.getInputStream();

        final int bufferSize = 1024;
        final char[] buffer = new char[bufferSize];
        final StringBuilder out = new StringBuilder();
        Reader in = new InputStreamReader(resourceInputStream, "UTF-8");
        for (; ; ) {
            int rsz = in.read(buffer, 0, buffer.length);
            if (rsz < 0)
                break;
            out.append(buffer, 0, rsz);
        }
        return out.toString();
    }

    @RequestMapping("insert")
    @Transactional
    public String insert(@RequestParam("method") String method, @RequestParam("type") String type, @RequestParam("amount") int amount) throws Exception
    {
        return testInsertService.insert(method, type, amount);
    }

    @RequestMapping("delete")
    @Transactional
    public String delete(@RequestParam("type") String type)
    {
        return testDeleteService.delete(type);
    }

    @RequestMapping("select")
    @Transactional
    public String select(@RequestParam("method") String method, @RequestParam("type") String type) throws Exception
    {
        return testSelectService.select(method, type);
    }

    @RequestMapping("count")
    @Transactional
    public String count(@RequestParam("type") String type)
    {
        return String.format("%d", testSelectService.count(type));
    }
}
