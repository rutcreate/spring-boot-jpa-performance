package com.rutcreate.jpabatch2.utils;

import java.util.HashMap;
import java.util.Map;

public class TimerManager
{
    protected Map<String, Timer> timers;

    protected String title;

    public TimerManager()
    {
        this.timers = new HashMap<>();
        this.title = "";
    }

    public TimerManager(String title)
    {
        this.timers = new HashMap<>();
        this.title = title;
    }

    public void start(String name)
    {
        Timer timer = timers.get(name);
        if (timer == null)
        {
            timer = new Timer();
            timers.put(name, timer);
        }
        timer.start();
    }

    public void stop(String name)
    {
        Timer timer = timers.get(name);
        if (timer == null)
        {
            timer = new Timer();
            timers.put(name, timer);
        }
        timer.stop();
    }

    public float getTime(String name)
    {
        Timer timer = timers.get(name);
        if (timer == null)
        {
            timer = new Timer();
            timers.put(name, timer);
        }
        return timer.getTime();
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("==============================<br />");
        sb.append("= ").append(this.title).append("<br />");
        sb.append("==============================<br />");
        for (Map.Entry<String, Timer> entry : this.timers.entrySet())
        {
            sb.append(entry.getKey() + ": " + entry.getValue().getTime() + "s<br />");
        }
        return sb.toString();
    }
}
