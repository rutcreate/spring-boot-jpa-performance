package com.rutcreate.jpabatch2.utils;

public class Timer
{
    protected Long startTime = 0L;

    protected Long stopTime = 0L;

    protected float elapsedTime = 0f;

    public void start()
    {
        startTime = System.nanoTime();
    }

    public void stop()
    {
        stopTime = System.nanoTime();
    }

    public float getTime()
    {
        elapsedTime = (stopTime - startTime) / 1000000000f;
        return elapsedTime;
    }
}
