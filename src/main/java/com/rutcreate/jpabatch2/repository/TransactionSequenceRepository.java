package com.rutcreate.jpabatch2.repository;

import com.rutcreate.jpabatch2.entity.TransactionSequence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TransactionSequenceRepository extends JpaRepository<TransactionSequence, Long>
{
    @Query(value = "SELECT COALESCE(MAX(id), 0) FROM test_transaction_sequence", nativeQuery = true)
    Long maxId();

    @Query(value = "SELECT COALESCE(MIN(id), 0) FROM test_transaction_sequence", nativeQuery = true)
    Long minId();
}
