package com.rutcreate.jpabatch2.repository;

import com.rutcreate.jpabatch2.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TransactionRepository extends JpaRepository<Transaction, Long>
{
    @Query(value = "SELECT COALESCE(MAX(id), 0) FROM test_transaction", nativeQuery = true)
    Long maxId();

    @Query(value = "SELECT COALESCE(MIN(id), 0) FROM test_transaction", nativeQuery = true)
    Long minId();
}
