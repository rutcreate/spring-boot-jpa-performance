package com.rutcreate.jpabatch2.entity;

import javax.persistence.*;

@Entity
@Table(name = "test_transaction")
public class Transaction
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String email;

    public Transaction() {}

    public Transaction(String name, String email)
    {
        this.name = name;
        this.email = email;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getEmail()
    {
        return this.email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }
}
