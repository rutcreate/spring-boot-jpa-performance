package com.rutcreate.jpabatch2.entity;

import javax.persistence.*;

@Entity
@Table(name = "test_transaction_no_id")
public class TransactionNoId
{
    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    private String name;

    private String email;

    public TransactionNoId()
    {}

    public TransactionNoId(String name, String email, Long id)
    {
        this.name = name;
        this.email = email;
        this.id = id;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getEmail()
    {
        return this.email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }
}
