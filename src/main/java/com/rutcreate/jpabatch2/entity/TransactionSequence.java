package com.rutcreate.jpabatch2.entity;

import javax.persistence.*;

@Entity
@Table(name = "test_transaction_sequence")
public class TransactionSequence
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transaction_sequence_seq")
    @SequenceGenerator(name = "transaction_sequence_seq")
    private Long id;

    private String name;

    private String email;

    public TransactionSequence()
    {}

    public TransactionSequence(String name, String email)
    {
        this.name = name;
        this.email = email;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getEmail()
    {
        return this.email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }
}
