package com.rutcreate.jpabatch2.service;

import com.rutcreate.jpabatch2.entity.Transaction;
import com.rutcreate.jpabatch2.entity.TransactionNoId;
import com.rutcreate.jpabatch2.entity.TransactionSequence;
import com.rutcreate.jpabatch2.repository.TransactionNoIdRepository;
import com.rutcreate.jpabatch2.repository.TransactionRepository;
import com.rutcreate.jpabatch2.repository.TransactionSequenceRepository;
import com.rutcreate.jpabatch2.utils.TimerManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class TestInsertService
{
    @PersistenceContext
    private EntityManager em;

    @Autowired
    private TransactionRepository t1repository;

    @Autowired
    private TransactionSequenceRepository t2repository;

    @Autowired
    private TransactionNoIdRepository t3repository;

    @Autowired
    private DataSource dataSource;

    /**
     *
     * @param type
     * @param amount
     * @return
     */
    public String insert(String method, String type, int amount) throws Exception
    {
        TimerManager timer = new TimerManager(type);

        timer.start("Overall");
        switch (method)
        {
            case "jdbc":
                this.insertJdbc(amount, type, timer);
                break;
            case "repo":
                this.insertRepo(amount, type, timer);
                break;
            case "em":
                this.insertEm(amount, type, timer);
        }
        timer.stop("Overall");

        return timer.toString();
    }

    /**
     *
     * @param amount
     * @param timer
     */
    @Transactional
    protected void insertJdbc(int amount, String type, TimerManager timer) throws Exception
    {
        Long maxId = 0L;
        if (type.equals("noid"))
        {
            maxId = t3repository.maxId();
        }
        Connection conn = dataSource.getConnection();
        conn.setAutoCommit(false);
        try
        {
            String sql = "";
            if (type.equals("normal"))
            {
                sql = "INSERT INTO test_transaction (name, email) VALUES (?, ?)";
            }
            else if (type.equals("sequence"))
            {
                throw new Exception("jdbc does not support sequence");
            }
            else if (type.equals("noid"))
            {
                sql = "INSERT INTO test_transaction_no_id (name, email, id) VALUES (?, ?, ?)";
            }

            PreparedStatement statement = conn.prepareStatement(sql);
            System.out.println(sql);
            for (int i = 0; i < amount; i++)
            {
                statement.setString(1, "test" + (maxId + i));
                statement.setString(2, "test" + (maxId + i) + "@gmail.com");
                if (type.equals("noid"))
                {
                    statement.setLong(3, maxId + i + 1);
                    // System.out.println("set id : " + (maxId + i + 1));
                }
                statement.addBatch();

                if (i > 0 && i % 500 == 0)
                {
                    statement.executeBatch();
                    conn.commit();
                    // System.out.println("inserted " + i + " records");
                }
            }
            statement.executeBatch();
            conn.commit();
            System.out.println("inserted " + amount + " records");
        }
        catch (SQLException e)
        {
            System.out.println("error: " + e.getMessage());
            conn.rollback();
            throw e;
        }
        finally {
            conn.close();
        }
    }

    /**
     *
     * @param amount
     * @param timer
     */
    @Transactional
    protected void insertRepo(int amount, String type, TimerManager timer)
    {
        if (type.equals("normal")) {
            List<Transaction> trans = new ArrayList<>();
            for (int i = 0; i < amount; i++) {
                Transaction transaction = new Transaction("normal" + i, "normal" + i + "@gmail.com");
                trans.add(transaction);

                if (i > 0 && i % 500 == 0) {
                    t1repository.saveAll(trans);
                    trans.clear();
                    System.out.println("inserted " + i + " records");
                }
            }
            t1repository.saveAll(trans);
            System.out.println("inserted " + amount + " records");
        }
        else if (type.equals("sequence"))
        {
            List<TransactionSequence> trans = new ArrayList<>();
            for (int i = 0; i < amount; i++) {
                TransactionSequence transaction = new TransactionSequence("normal" + i, "normal" + i + "@gmail.com");
                trans.add(transaction);

                if (i > 0 && i % 500 == 0) {
                    t2repository.saveAll(trans);
                    trans.clear();
                    // System.out.println("inserted " + i + " records");
                }
            }
            t2repository.saveAll(trans);
            System.out.println("inserted " + amount + " records");
        }
        else if (type.equals("noid"))
        {
            Long maxId = t3repository.maxId();
            List<TransactionNoId> trans = new ArrayList<>();
            for (int i = 0; i < amount; i++) {
                TransactionNoId transaction = new TransactionNoId("normal" + (maxId + i), "normal" + (maxId + i) + "@gmail.com", maxId + i + 1);
                trans.add(transaction);

                if (i > 0 && i % 500 == 0) {
                    t3repository.saveAll(trans);
                    trans.clear();
                    // System.out.println("inserted " + i + " records");
                }
            }
            t3repository.saveAll(trans);
            System.out.println("inserted " + amount + " records");
        }
    }

    /**
     *
     * @param amount
     * @param timer
     */
    @Transactional
    protected void insertEm(int amount, String type, TimerManager timer)
    {
        Long maxId = 0L;
        if (type.equals("noid"))
        {
            maxId = t3repository.maxId();
        }
        try
        {
            for (int i = 0; i < amount; i++)
            {
                if (type.equals("normal"))
                {
                    Transaction transaction = new Transaction("normal" + i, "normal" + i + "@gmail.com");
                    em.persist(transaction);
                }
                else if (type.equals("sequence")) {
                    TransactionSequence transaction = new TransactionSequence("normal" + i, "normal" + i + "@gmail.com");
                    em.persist(transaction);
                }
                else if (type.equals("noid")) {
                    TransactionNoId transaction = new TransactionNoId("normal" + (maxId + i), "normal" + (maxId + i) + "@gmail.com", maxId + i + 1);
                    em.persist(transaction);
                }

                if (i > 0 && i % 500 == 0)
                {
                    em.flush();
                    em.clear();
                    // System.out.println("inserted " + i + " records");
                }
            }

            em.flush();
            em.clear();
            System.out.println("inserted " + amount + " records");
        }
        catch (RuntimeException e)
        {
            throw e;
        }
        finally
        {
            em.close();
        }
    }
}
