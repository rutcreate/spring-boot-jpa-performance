package com.rutcreate.jpabatch2.service;

import com.rutcreate.jpabatch2.entity.Transaction;
import com.rutcreate.jpabatch2.entity.TransactionNoId;
import com.rutcreate.jpabatch2.entity.TransactionSequence;
import com.rutcreate.jpabatch2.repository.TransactionNoIdRepository;
import com.rutcreate.jpabatch2.repository.TransactionRepository;
import com.rutcreate.jpabatch2.repository.TransactionSequenceRepository;
import com.rutcreate.jpabatch2.utils.TimerManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class TestSelectService
{
    @Autowired
    private TransactionRepository t1repository;

    @Autowired
    private TransactionSequenceRepository t2repository;

    @Autowired
    private TransactionNoIdRepository t3repository;

    @Autowired
    private DataSource dataSource;

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public String select(String method, String type) throws Exception
    {
        TimerManager timer = new TimerManager("SELECT");

        timer.start("Overall");
        switch (method)
        {
            case "jdbc":
                this.selectJdbc(type, timer);
                break;
            case "jdbc-single":
                this.selectJdbcSingle(type, timer);
                break;
            case "repo":
                this.selectRepo(type, timer);
                break;
            case "repo-single":
                this.selectRepoSingle(type, timer);
                break;
            case "em":
                this.selectEm(type, timer);
                break;
            case "em-single":
                this.selectEmSingle(type, timer);
                break;
        }
        timer.stop("Overall");

        return timer.toString();
    }

    @Transactional
    protected void selectJdbc(String type, TimerManager timer) throws SQLException
    {
        Connection conn = dataSource.getConnection();
        conn.setAutoCommit(false);
        try
        {
            String sql = "";
            if (type.equals("normal"))
            {
                sql = "SELECT * FROM test_transaction";
            }
            else if (type.equals("sequence"))
            {
                sql = "SELECT * FROM test_transaction_sequence";
            }
            else if (type.equals("noid"))
            {
                sql = "SELECT * FROM test_transaction_no_id";
            }
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            int i = 0;
            while (rs.next()) {
                Long id = rs.getLong("id");
                String name = rs.getString("name");
                String email = rs.getString("email");

                if (i > 0 && i % 500 == 0)
                {
                    System.out.println("loaded " + i + " records");
                }
                i++;
            }
        }
        catch (SQLException e)
        {
            System.out.println("error: " + e.getMessage());
            conn.rollback();
            throw e;
        }
        finally {
            conn.close();
        }
    }

    @Transactional
    protected void selectJdbcSingle(String type, TimerManager timer) throws SQLException
    {
        Connection conn = dataSource.getConnection();
        conn.setAutoCommit(false);
        try
        {
            String sql = "";
            Long min = 0L;
            Long max = 0L;
            if (type.equals("normal"))
            {
                sql = "SELECT * FROM test_transaction WHERE id = ?";
                max = t1repository.maxId();
                min = t1repository.minId();
                System.out.println(min + "-" + max);
            }
            else if (type.equals("sequence"))
            {
                sql = "SELECT * FROM test_transaction_sequence WHERE id = ?";
                max = t2repository.maxId();
                min = t2repository.minId();
                System.out.println(min + "-" + max);
            }
            else if (type.equals("noid"))
            {
                sql = "SELECT * FROM test_transaction_no_id WHERE id = ?";
                max = t2repository.maxId();
                min = t2repository.minId();
                System.out.println(min + "-" + max);
            }
            PreparedStatement statement = conn.prepareStatement(sql);
            for (; min <= max; min++)
            {
                statement.setLong(1, min);
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    Long id = rs.getLong("id");
                    String name = rs.getString("name");
                    String email = rs.getString("email");
                    // System.out.println(id + ", " + name + ", " + email);
                }
            }
        }
        catch (SQLException e)
        {
            System.out.println("error: " + e.getMessage());
            conn.rollback();
            throw e;
        }
        finally {
            conn.close();
        }
    }

    @Transactional
    protected void selectRepo(String type, TimerManager timer)
    {
        if (type.equals("normal")) {
            List<Transaction> trans = t1repository.findAll();
            for (Transaction transaction : trans)
            {
                Long id = transaction.getId();
                String name = transaction.getName();
                String email = transaction.getEmail();
            }
        }
        else if (type.equals("sequence")) {
            List<TransactionSequence> trans = t2repository.findAll();
            for (TransactionSequence transaction : trans)
            {
                Long id = transaction.getId();
                String name = transaction.getName();
                String email = transaction.getEmail();
            }
        }
        else if (type.equals("noid")) {
            List<TransactionNoId> trans = t3repository.findAll();
            for (TransactionNoId transaction : trans)
            {
                Long id = transaction.getId();
                String name = transaction.getName();
                String email = transaction.getEmail();
            }
        }
    }

    @Transactional
    protected void selectRepoSingle(String type, TimerManager timer)
    {
        if (type.equals("normal")) {
            Long max = t1repository.maxId();
            Long min = t1repository.minId();
            System.out.println(min + "-" + max);
            List<Transaction> trans = new ArrayList<>();
            for (; min <= max; min++)
            {
                Transaction transaction = t1repository.getOne(min);
                trans.add(transaction);
            }
            for (Transaction transaction : trans)
            {
                Long id = transaction.getId();
                String name = transaction.getName();
                String email = transaction.getEmail();
            }
        }
        else if (type.equals("sequence")) {
            Long max = t2repository.maxId();
            Long min = t2repository.minId();
            System.out.println(min + "-" + max);
            List<TransactionSequence> trans = new ArrayList<>();
            for (; min <= max; min++)
            {
                TransactionSequence transaction = t2repository.getOne(min);
                trans.add(transaction);
            }
            for (TransactionSequence transaction : trans)
            {
                Long id = transaction.getId();
                String name = transaction.getName();
                String email = transaction.getEmail();
            }
        }
        else if (type.equals("noid")) {
            Long max = t3repository.maxId();
            Long min = t3repository.minId();
            System.out.println(min + "-" + max);
            List<TransactionNoId> trans = new ArrayList<>();
            for (; min <= max; min++)
            {
                TransactionNoId transaction = t3repository.getOne(min);
                trans.add(transaction);
            }
            for (TransactionNoId transaction : trans)
            {
                Long id = transaction.getId();
                String name = transaction.getName();
                String email = transaction.getEmail();
            }
        }
    }

    @Transactional
    protected void selectEm(String type, TimerManager timer)
    {
        if (type.equals("normal")) {
            Query query = em.createNativeQuery("SELECT id, name, email FROM test_transaction");
            List<Object[]> result = query.getResultList();
            for (Object[] row : result)
            {
                Long id = Long.parseLong(row[0].toString());
                String name = row[1].toString();
                String email = row[2].toString();
            }
        }
        else if (type.equals("sequence"))
        {
            Query query = em.createNativeQuery("SELECT id, name, email FROM test_transaction_sequence");
            List<Object[]> result = query.getResultList();
            for (Object[] row : result)
            {
                Long id = Long.parseLong(row[0].toString());
                String name = row[1].toString();
                String email = row[2].toString();
            }
        }
        else if (type.equals("noid"))
        {
            Query query = em.createNativeQuery("SELECT id, name, email FROM test_transaction_noid");
            List<Object[]> result = query.getResultList();
            for (Object[] row : result)
            {
                Long id = Long.parseLong(row[0].toString());
                String name = row[1].toString();
                String email = row[2].toString();
            }
        }
    }

    @Transactional
    protected void selectEmSingle(String type, TimerManager timer)
    {
        if (type.equals("normal")) {
            Long max = t1repository.maxId();
            Long min = t1repository.minId();
            System.out.println(min + "-" + max);
            
            for (; min <= max; min++)
            {
                Query query = em.createNativeQuery("SELECT id, name, email FROM test_transaction WHERE id = :id");
                query.setParameter("id", min);
                List<Object[]> result = query.getResultList();
                for (Object[] row : result)
                {
                    Long id = Long.parseLong(row[0].toString());
                    String name = row[1].toString();
                    String email = row[2].toString();
                }
            }
        }
        else if (type.equals("sequence"))
        {
            Long max = t2repository.maxId();
            Long min = t2repository.minId();
            System.out.println(min + "-" + max);

            for (; min <= max; min++)
            {
                Query query = em.createNativeQuery("SELECT id, name, email FROM test_transaction_sequence WHERE id = :id");
                query.setParameter("id", min);
                List<Object[]> result = query.getResultList();
                for (Object[] row : result)
                {
                    Long id = Long.parseLong(row[0].toString());
                    String name = row[1].toString();
                    String email = row[2].toString();
                }
            }
        }
        else if (type.equals("noid"))
        {
            Long max = t3repository.maxId();
            Long min = t3repository.minId();
            System.out.println(min + "-" + max);

            for (; min <= max; min++)
            {
                Query query = em.createNativeQuery("SELECT id, name, email FROM test_transaction_noid WHERE id = :id");
                query.setParameter("id", min);
                List<Object[]> result = query.getResultList();
                for (Object[] row : result)
                {
                    Long id = Long.parseLong(row[0].toString());
                    String name = row[1].toString();
                    String email = row[2].toString();
                }
            }
        }
    }

    @Transactional
    public long count(String type)
    {
        if (type.equals("normal")) {
            return t1repository.count();
        }
        else if (type.equals("sequence")) {
            return t2repository.count();
        }
        else if (type.equals("noid")) {
            return t3repository.count();
        }
        return 0;
    }
}
