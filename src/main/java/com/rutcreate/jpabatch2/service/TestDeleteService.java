package com.rutcreate.jpabatch2.service;

import com.rutcreate.jpabatch2.repository.TransactionNoIdRepository;
import com.rutcreate.jpabatch2.repository.TransactionRepository;
import com.rutcreate.jpabatch2.repository.TransactionSequenceRepository;
import com.rutcreate.jpabatch2.utils.TimerManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TestDeleteService {

    @Autowired
    private TransactionRepository t1repository;

    @Autowired
    private TransactionSequenceRepository t2repository;

    @Autowired
    private TransactionNoIdRepository t3repository;

    @Transactional
    public String delete(String type)
    {
        TimerManager timer = new TimerManager("Delete");
        timer.start("Overall");
        if (type.equals("normal"))
        {
            t1repository.deleteAll();
        }
        else if (type.equals("sequence"))
        {
            t2repository.deleteAll();
        }
        else if (type.equals("noid"))
        {
            t3repository.deleteAll();
        }
        timer.stop("Overall");
        return timer.toString();
    }
}
